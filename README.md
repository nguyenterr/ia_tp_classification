##################    
#   Projet IA :  #
##################
Groupe: Terry Nguyen, Aurélien Lamontre

Nous avons choisi les méthodes suivantes :

    - naivesBayes

    - DecisionTree

    - sgdClassifer

    - k-Nearest

    - Linear Regression


La meilleure méthode que nous avons trouvée est le "Décision Tree" car elle possède les meilleures, par rapport aux autres méthodes, et optimales statistiques.

 precision    recall  f1-score   support

           0       0.99      0.99      0.99     31843
           1       0.98      0.97      0.98     13713

    accuracy                           0.99     45556
   macro avg       0.98      0.98      0.98     45556
weighted avg       0.99      0.99      0.99     45556

Nous avons fait un histogramme pour comparer les différentes méthodes.

Chaque méthode possède son diagramme de Kiviat (diagramme radar).

Un diagramme de Kiviat regroupe 4 sur les 5 méthodes pour une meilleure observation. 



Pour exécuter le code tapez :

python3 separate.py

