import numpy as np
from sklearn.linear_model import SGDRegressor
import pandas as pd
import pandas_profiling
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.cluster import KMeans
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt2
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.datasets import load_iris
from warnings import simplefilter
import warnings
warnings.filterwarnings('ignore')

simplefilter(action='ignore', category=FutureWarning)

df = pd.read_csv('antivirus_dataset.csv')
t_drop=[
'Name',
'md5',
'MinorImageVersion',
'MinorSubsystemVersion',
'SectionsMeanRawsize',
'SectionsMeanVirtualsize',
'SectionsMinVirtualsize',
'SizeOfHeapCommit',
'SizeOfOptionalHeader',
'ExportNb',
'ImportsNbOrdinal',
'LoadConfigurationSize',
'LoaderFlags',
'MajorImageVersion',
'MinorLinkerVersion',
'SizeOfUninitializedData',
'legitimate'
]
new_df=df.drop(columns=t_drop)

x=new_df
y=df[['legitimate']]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)


def train_using_gini(x_train,x_test,y_train): #Training with giniIndex
          clf_gini = DecisionTreeClassifier(criterion = "gini", random_state = 100,max_depth=3, min_samples_leaf=5)
          clf_gini.fit(x_train,y_train)
          return clf_gini

def train_using_entropy(x_train,x_test,y_train): #Training with entropy
          clf_entropy=DecisionTreeClassifier(
          criterion = "entropy", random_state = 100,
          max_depth = 3, min_samples_leaf = 5)
          clf_entropy.fit(x_train,y_train)
          return clf_entropy

def prediction(x_test,clf_object): #Making predictions
          y_pred=clf_object.predict(x_test)
          return y_pred

def cal_accuracy(y_test,y_pred): #Calculating accuracy
          print(confusion_matrix(y_test,y_pred))
          print(accuracy_score(y_test,y_pred)*100)
          print(classification_report(y_test,y_pred))

def sgdClassifer(x_train,y_train,y_test):
	clf = linear_model.SGDClassifier(max_iter=1000, tol=1e-3)
	clf.fit(x_train, y_train.values.ravel())
	y_pred=clf.predict(x_test)
	return y_pred

def linearRegression(x_train,y_train,x_test):
	regr=linear_model.LinearRegression() #Linear regression object
	regr.fit(x_train,y_train.values.ravel())
	y_pred=regr.predict(x_test)
	return y_pred

def naivesBayes(x_train,y_train,x_test,y_test):
	gnb=GaussianNB()
	mnb=MultinomialNB()
	y_pred_gnb=gnb.fit(x_train,y_train.values.ravel()).predict(x_test)
	return y_pred_gnb

def iris(x_train,y_train,x_test,y_test):
    logreg=LogisticRegression()
    logreg.fit(x_train,y_train.values.ravel())
    y_pred=logreg.predict(x_test)
    return y_pred



y_pred=naivesBayes(x_train,y_train,x_test,y_test)
precision,recall,fscore,support=score(y_test,y_pred,average='macro')
accurancy=accuracy_score(y_test, y_pred, normalize=True)

y_pred2=prediction(x_test,train_using_entropy(x_train,x_test,y_train))
precision2,recall2,fscore2,support2=score(y_test,y_pred2,average='macro')


y_pred3=sgdClassifer(x_train,y_train,y_test)
precision3,recall3,fscore3,support3=score(y_test,y_pred3,average='macro')

y_pred4=iris(x_train,y_train,x_test,y_test)
precision4,recall4,fscore4,support4=score(y_test,y_pred4,average='macro')

y_pred5=linearRegression(x_train,y_train,x_test)
precision5,recall5,fscore5,support5=score(y_test,y_pred5.round(),average='macro')


# x-coordinates of left sides of bars
left = [1, 2, 3,4,5]

# heights of bars
height = [precision,precision2,precision3,precision4,precision5]

# labels for bars
tick_label = ['naivesBayes', 'DecisionTree', 'sgdClassifer','k-Nearest','linear Regression']

# plotting a bar chart
plt.bar(left, height, tick_label = tick_label,
        width = 0.8, color = ['red', 'green','blue','orange','yellow'])

# naming the x-axis
plt.xlabel('x - axis')
# naming the y-axis
plt.ylabel('y - axis')
# plot title
plt.title('Comparaison de la précision des méthodes')

# function to show the plot
plt.show()

Attributes = ["Precision","Recall","Score"]
data1=[precision,recall,fscore]
data2=[precision2,recall2,fscore2]
data3=[precision3,recall3,fscore3]
data4=[precision4,recall4,fscore4]
data5=[precision5,recall5,fscore5]

data1 += data1 [:1]
data2 += data2 [:1]
data3 += data3 [:1]
data4 += data4 [:1]
data5 += data5 [:1]
    
angles = [n / 3 * 2 * 3.14 for n in range(3)]
angles += angles [:1]
    
ax = plt.subplot(111, polar=True)

plt.xticks(angles[:-1],Attributes)
ax.plot(angles,data1)
ax.plot(angles,data2)
ax.plot(angles,data3)
ax.plot(angles,data4)

ax.fill(angles, data1, "blue", alpha=0.1)
ax.fill(angles, data2, "red", alpha=0.1)
ax.fill(angles, data3, "green", alpha=0.1)
ax.fill(angles, data4, "yellow", alpha=0.1)
plt.legend(['naivesBayes', 'DecisionTree', 'sgdClassifer','k-Nearest'])

ax.set_title("Comparaison des méthodes ")
plt.show()
